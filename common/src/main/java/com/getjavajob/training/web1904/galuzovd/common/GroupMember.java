package com.getjavajob.training.web1904.galuzovd.common;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "groupmembers")
public class GroupMember {

    @Id
    private int id;
    private String role;
    private int groupId;
    private int memberId;
    @Transient
    private boolean isMember;

    public GroupMember() {
    }

    public GroupMember(String role, int groupId, int memberId) {
        this.role = role;
        this.groupId = groupId;
        this.memberId = memberId;
    }

    public boolean isMember() {
        return isMember;
    }

    public void setMember(boolean member) {
        isMember = member;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public int getMemberId() {
        return memberId;
    }

    public void setMemberId(int memberId) {
        this.memberId = memberId;
    }
}