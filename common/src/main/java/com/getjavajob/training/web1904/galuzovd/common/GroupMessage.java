package com.getjavajob.training.web1904.galuzovd.common;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "groupmessage")
public class GroupMessage {
    @Id
    private int id;
    private int groupId;
    private int memberId;
    private String message;

    public GroupMessage(int memberId, int groupId, String message) {
        this.groupId = groupId;
        this.memberId = memberId;
        this.message = message;
    }

    public GroupMessage() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public int getMemberId() {
        return memberId;
    }

    public void setMemberId(int memberId) {
        this.memberId = memberId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}