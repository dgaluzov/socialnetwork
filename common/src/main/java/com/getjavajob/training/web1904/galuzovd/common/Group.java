package com.getjavajob.training.web1904.galuzovd.common;

import javax.persistence.*;

@Entity
@Table(name = "groups")
public class Group {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    private String description;
    private int ownerId;
    private String photo;

    public Group(int ownerId, String name, String description, String photo) {
        this.name = name;
        this.description = description;
        this.photo = photo;
        this.ownerId = ownerId;
    }

    public Group() {
    }

    public String getPhoto() {
        return photo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public int getOwnerId() {
        return ownerId;
    }
}