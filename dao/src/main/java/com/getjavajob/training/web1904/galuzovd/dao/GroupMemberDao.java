package com.getjavajob.training.web1904.galuzovd.dao;

import com.getjavajob.training.web1904.galuzovd.common.GroupMember;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface GroupMemberDao extends CrudRepository<GroupMember, Integer> {
    @Modifying
    @Query("DELETE from GroupMember WHERE memberId = :memberId and groupId = :groupId")
    void deleteGroupMember(@Param("memberId") Integer memberId, @Param("groupId") Integer groupId);

    @Modifying
    @Query("update GroupMember gm set gm.role = :role where gm.memberId = :memberId")
    void updateMembersRole(@Param("memberId") Integer id, @Param("role") String role);

    @Query("select gm.role from GroupMember gm where gm.groupId=:groupId and gm.memberId=:userId")
    String selectUserRole(@Param("groupId") Integer groupId, @Param("userId") int userId);

    @Query("select gm from GroupMember gm where gm.groupId=:groupId")
    List<GroupMember> selectGroupMembers(@Param("groupId") Integer groupId);
}

