package com.getjavajob.training.web1904.galuzovd.dao;

import com.getjavajob.training.web1904.galuzovd.common.Group;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import java.util.List;

@SuppressWarnings("All")
@Component
public interface GroupDao extends CrudRepository<Group, Integer> {
    @Modifying
    @Query("DELETE from Group WHERE id = :id")
    void deleteGroupById(@Param("id") Integer id);

    @Modifying
    @Query("update Group g SET g.ownerId=:id where g.id = :groupId")
    void updateOwnerId(@Param("id") Integer id, @Param("groupId") Integer groupId);

    @Query("select gr from Group gr where gr.id=:groupId")
    Group get(@Param("groupId") Integer groupId);

    @Query("select gr from Group gr where lower(gr.name) like :search")
    List<Group> searchGroup(@Param("search") String search);
}