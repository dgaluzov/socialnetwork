package com.getjavajob.training.web1904.galuzovd.dao;

import com.getjavajob.training.web1904.galuzovd.common.Account;
import com.getjavajob.training.web1904.galuzovd.common.Friend;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface FriendDao extends CrudRepository<Friend, Integer> {
    @Modifying
    @Query("update Friend fr set fr.relationType='Accepted' where fr.accountId =:senderId and fr.friendId=:recipientId")
    void confirmSenderRequest(@Param("senderId") Integer senderId, @Param("recipientId") int recipientId
    );

    @Modifying
    @Query("update Friend fr set fr.relationType='Accepted' where fr.accountId =:recipientId and fr .friendId=:senderId")
    void confirmRecipientRequest(@Param("senderId") Integer senderId, @Param("recipientId") int recipientId
    );

    @Modifying
    @Query("delete from  Friend fr where fr.accountId=:senderId and fr.friendId=:recipientId")
    void deleteFromFriendsSender(@Param("senderId") Integer senderId, @Param("recipientId") int recipientId
    );

    @Modifying
    @Query("delete from  Friend fr where fr.accountId=:recipientId and fr.friendId=:senderId")
    void deleteFromFriendsRecipient(@Param("senderId") Integer senderId, @Param("recipientId") int recipientId
    );

    @Query("select ac from Account ac JOIN Friend fr on ac.id=fr.accountId where fr.friendId =:id")
    List<Account> selectFriends(@Param("id") Integer id);
}