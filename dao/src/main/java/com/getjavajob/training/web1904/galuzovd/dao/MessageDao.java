package com.getjavajob.training.web1904.galuzovd.dao;

import com.getjavajob.training.web1904.galuzovd.common.Message;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface MessageDao extends CrudRepository<Message, Integer> {
    @Query("select msg from Message msg where msg.accountId = :senderId and msg.friendId= :recipientId order by msg" +
            ".messageDate asc ")
    List<Message> selectMessage(@Param("senderId") Integer senderId, @Param("recipientId") int recipientId
    );
}
