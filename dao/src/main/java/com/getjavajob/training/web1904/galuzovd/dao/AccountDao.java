package com.getjavajob.training.web1904.galuzovd.dao;

import com.getjavajob.training.web1904.galuzovd.common.Account;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface AccountDao extends CrudRepository<Account, Integer> {

    @Modifying
    @Query("update Account ac set ac.name =:name, ac.surname =:surname ,ac.email =:email, ac.password=:password," +
            "ac.userPic=:userPic where ac.id=:id")
    void updateAccount(@Param("id") Integer id, @Param("name") String name, @Param("surname") String surname,
                       @Param("email") String email, @Param("password") String password,
                       @Param("userPic") String userPic);

    @Query("select fr.relationType from Friend  fr where fr.friendId=:friendId and fr.accountId =:id")
    String selectRelationType(@Param("id") Integer id, @Param("friendId") int friendId);

    @Query("select ac from Account ac where ac.email=:email")
    Account searchUser(@Param("email") String email);

    @Query("select ac from Account ac where lower(ac.name) like :query or lower(ac.surname) like :query")
    List<Account> searchUsers(@Param("query") String query);

    @Query("select ac from Account ac where ac.email=:email")
    List<Account> isDuplicateEmail(@Param("email") String email);
}


