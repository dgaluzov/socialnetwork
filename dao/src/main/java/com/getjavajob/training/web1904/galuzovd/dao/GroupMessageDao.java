package com.getjavajob.training.web1904.galuzovd.dao;

import com.getjavajob.training.web1904.galuzovd.common.GroupMessage;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface GroupMessageDao extends CrudRepository<GroupMessage, Integer> {
    @Query("select gmg from GroupMessage gmg where gmg.groupId=:groupId")
    List<GroupMessage> selectGroupMessages(@Param("groupId") int groupId);
}