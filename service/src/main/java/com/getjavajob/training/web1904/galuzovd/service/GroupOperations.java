package com.getjavajob.training.web1904.galuzovd.service;

import com.getjavajob.training.web1904.galuzovd.common.Group;
import com.getjavajob.training.web1904.galuzovd.common.GroupMember;
import com.getjavajob.training.web1904.galuzovd.common.GroupMessage;
import com.getjavajob.training.web1904.galuzovd.dao.GroupDao;
import com.getjavajob.training.web1904.galuzovd.dao.GroupMemberDao;
import com.getjavajob.training.web1904.galuzovd.dao.GroupMessageDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Component
@Transactional
public class GroupOperations {
    @Autowired
    private GroupDao groupDao;
    @Autowired
    private GroupMemberDao groupMemberDao;
    @Autowired
    private GroupMessageDao groupMessageDao;

    public int createGroup(Group group) {
        Group newGroup = groupDao.save(group);
        return newGroup.getId();
    }

    public String selectUserRole(int userId, int groupId) {
        return groupMemberDao.selectUserRole(userId, groupId);
    }

    public List<GroupMember> selectGroupMembers(int groupId) {
        return groupMemberDao.selectGroupMembers(groupId);
    }

    public List<GroupMessage> selectGroupMessages(int groupId) {
        return groupMessageDao.selectGroupMessages(groupId);
    }

    public void deleteGroup(int id) {
        groupDao.deleteGroupById(id);
    }

    public void updateGroup(Group group) {
        groupDao.save(group);
    }

    public Group getGroup(int groupId) {
        return groupDao.get(groupId);
    }

    public GroupMessage sendMessageToGroup(GroupMessage groupMessage) {
        return groupMessageDao.save(groupMessage);
    }

    public GroupMember addGroupMember(GroupMember groupMember) {
        return groupMemberDao.save(groupMember);
    }

    public void updateMembersRole(int memberId, String role) {
        groupMemberDao.updateMembersRole(memberId, role);
    }

    public void deleteGroupMember(int groupId, int userId) {
        groupMemberDao.deleteGroupMember(userId, groupId);
    }
}