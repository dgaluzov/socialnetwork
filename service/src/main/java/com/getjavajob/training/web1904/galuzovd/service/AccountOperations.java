package com.getjavajob.training.web1904.galuzovd.service;

import com.getjavajob.training.web1904.galuzovd.common.Account;
import com.getjavajob.training.web1904.galuzovd.common.Friend;
import com.getjavajob.training.web1904.galuzovd.common.Message;
import com.getjavajob.training.web1904.galuzovd.dao.AccountDao;
import com.getjavajob.training.web1904.galuzovd.dao.FriendDao;
import com.getjavajob.training.web1904.galuzovd.dao.MessageDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

@Component
public class AccountOperations {
    @Autowired
    private FriendDao friendDao;
    @Autowired
    private AccountDao accountDao;

    @Autowired
    private MessageDao messageDao;

    @Transactional
    public void sendRequest(int senderId, int recipientId) {
        Friend sender = new Friend();
        sender.setAccountId(senderId);
        sender.setFriendId(recipientId);
        sender.setRelationType("requestSent");
        friendDao.save(sender);
        Friend recipient = new Friend();
        recipient.setAccountId(recipientId);
        recipient.setFriendId(senderId);
        recipient.setRelationType("requestReceived");
        friendDao.save(recipient);
    }

    @Transactional
    public void confirmFriend(int senderId, int recipientId) {
        friendDao.confirmRecipientRequest(senderId, recipientId);
        friendDao.confirmSenderRequest(senderId, recipientId);
    }

    @Transactional
    public void deleteFriend(int senderId, int recipientId) {
        friendDao.deleteFromFriendsRecipient(senderId, recipientId);
        friendDao.deleteFromFriendsSender(senderId, recipientId);
    }

    @Transactional
    public void sendMessageSender(int senderId, int recipientId, String msgText) {
        Message senderMessage = new Message();
        senderMessage.setFriendId(recipientId);
        senderMessage.setAccountId(senderId);
        senderMessage.setMessageText(msgText);
        senderMessage.setStatus("Message sent");
        senderMessage.setMessageDate(LocalDateTime.now());
        messageDao.save(senderMessage);
    }

    @Transactional
    public void sendMessageRecipient(int senderId, int recipientId, String msgText) {
        Message recipientMessage = new Message();
        recipientMessage.setFriendId(senderId);
        recipientMessage.setAccountId(recipientId);
        recipientMessage.setMessageText(msgText);
        recipientMessage.setStatus("Message received");
        recipientMessage.setMessageDate(LocalDateTime.now());
        messageDao.save(recipientMessage);
    }

    public List<Account> searchUsers(String query) {
        String modifiedQuery = "%" + query.toLowerCase() + "%";
        return accountDao.searchUsers(modifiedQuery);
    }

    //todo �������
    @Modifying
    @Transactional
    public int createAccount(String name, String surname, String email, String password, String link) {
        password = new BCryptPasswordEncoder().encode(password);
        Account account = new Account();
        account.setName(name);
        account.setSurname(surname);
        account.setEmail(email);
        account.setPassword(password);
        account.setUserPic(link);
        account.setSecurityRole("ROLE_USER");
        Account createdAccount = accountDao.save(account);
        return createdAccount.getId();
    }

    public void deleteAccount(int id) {
        accountDao.deleteById(id);
    }

    public Account getAccount(int id) {
        return accountDao.findById(id).orElse(null);
    }

    public String selectRelationType(int id, int friendId) {
        String result = accountDao.selectRelationType(id, friendId);
        if (result == null) {
            return "";
        } else {
            return result;
        }
    }

    public Account searchUser(String email) {
        return accountDao.searchUser(email);
    }

    public boolean isDuplicate(String email) {
        List<Account> accounts = accountDao.isDuplicateEmail(email);
        return !accounts.isEmpty();
    }

    public List<Account> selectFriends(int id) {
        return friendDao.selectFriends(id);
    }

    public List<Message> selectMessages(int id, int recepientId) {
        return messageDao.selectMessage(id, recepientId);
    }

    // TODO: 08.09.2019 cltkfyj
    @Transactional
    public void updateProfile(int id, String name, String surname, String email, String password, String link) {
        password = new BCryptPasswordEncoder().encode(password);
        accountDao.updateAccount(id, name, surname, email, password, link);
    }
}
