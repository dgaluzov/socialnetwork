package com.getjavajob.training.web1904.galuzovd.service.tests;

import com.getjavajob.training.web1904.galuzovd.common.*;
import com.getjavajob.training.web1904.galuzovd.dao.*;
import com.getjavajob.training.web1904.galuzovd.service.AccountOperations;
import com.getjavajob.training.web1904.galuzovd.service.GroupOperations;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:service-context.xml"})
public class ServiceTest {
    @Autowired
    private AccountOperations accountOperations;
    @Autowired
    private GroupOperations groupOperations;
    @Autowired
    private GroupMemberDao groupMemberDao;
    @Autowired
    private AccountDao accountDao;
    @Autowired
    private FriendDao friendDao;
    @Autowired
    private MessageDao messageDao;
    @Autowired
    private GroupMessageDao groupMessageDao;

    private int accountId;
    private int senderId = 1;
    private int recipientId = 2;
    private int groupId;
    private GroupMessage groupMessage;
    private GroupMember groupMember;

    @Before
    public void createEntities() {
        this.accountId = accountOperations.createAccount("testName", "testSurname", "testEmail",
                "testPassword", "testLink");
        Group group = new Group(1, "testName", "testDescription", "testPhoto");
        this.groupId = groupOperations.createGroup(group);
        Friend friend = new Friend();
        friend.setAccountId(senderId);
        friend.setFriendId(recipientId);
        friend.setRelationType("Accepted");
        friendDao.save(friend);
        GroupMember groupMember = new GroupMember();
        groupMember.setRole("test");
        groupMember.setGroupId(1);
        groupMember.setMemberId(1);
        this.groupMember = groupMemberDao.save(groupMember);
        GroupMessage groupMessage = new GroupMessage();
        groupMessage.setGroupId(groupId);
        groupMessage.setMemberId(1);
        groupMessage.setMessage("test");
        this.groupMessage = groupMessageDao.save(groupMessage);
    }

    @Test
    public void updateAccount() {
        accountOperations.updateProfile(accountId, "testName1", "testSurname", "testEmail",
                "testPassword", "testLink");
        String expected = "testName1";
        Account account = accountDao.findById(accountId).orElse(null);
        String actual = account.getName();
        assertEquals(expected, actual);
    }

    @Test
    public void sendFriendRequest() {
        accountOperations.sendRequest(senderId, recipientId);
        Friend sender = friendDao.findById(senderId).orElse(null);
        Friend recipient = friendDao.findById(recipientId).orElse(null);
        assertEquals(1, sender.getAccountId());
        assertEquals(2, recipient.getAccountId());
    }

    @Test
    public void confirmFriend() {
        accountOperations.confirmFriend(1, 2);
        Friend sender = friendDao.findById(senderId).orElse(null);
        Friend recipient = friendDao.findById(recipientId).orElse(null);
        assertEquals("Accepted", sender.getRelationType());
        assertEquals("Accepted", recipient.getRelationType());
    }

    @Test
    public void deleteFriend() {
        accountOperations.deleteFriend(1, 2);
        Friend sender = friendDao.findById(senderId).orElse(null);
        Friend recipient = friendDao.findById(recipientId).orElse(null);
        assertNull(sender);
        assertNull(recipient);
    }

    @Test
    public void sendMessage() {
        accountOperations.sendMessageSender(1, 2, "Hello");
        accountOperations.sendMessageRecipient(1, 2, "Hello");
        Message sentMessage = messageDao.findById(senderId).orElse(null);
        Message receivedMessage = messageDao.findById(recipientId).orElse(null);
        assertEquals("Hello", sentMessage.getMessageText());
        assertEquals("Hello", receivedMessage.getMessageText());
    }

    @Test
    public void searchUsers() {
        accountOperations.searchUsers("test");
        Account account = accountDao.findById(accountId).orElse(null);
        assertEquals("testName", account.getName());
    }

    @Test
    public void getAccount() {
        Account account = accountOperations.getAccount(1);
        assertEquals(1, account.getId());
    }

    @Test
    public void selectRelationType() {
        String actual = accountOperations.selectRelationType(senderId, recipientId);
        assertEquals("Accepted", actual);
    }

    @Test
    public void searchUser() {
        Account foundUser = accountOperations.searchUser("testEmail");
        assertEquals("testEmail", foundUser.getEmail());
    }

    @Test
    public void isDuplicate() {
        boolean isDuplicate = accountOperations.isDuplicate("testEmail");
        assertTrue(isDuplicate);
    }

    @Test
    public void selectFriends() {
        List<Account> accounts = accountOperations.selectFriends(senderId);
        Account account = accounts.get(0);
        assertNotNull(account);
    }

    @Test
    public void selectMessages() {
        List<Message> messages = accountOperations.selectMessages(senderId, recipientId);
        assertNotNull(messages);
    }

    @Test
    public void selectUserRole() {
        String userRole = groupOperations.selectUserRole(senderId, groupId);
        assertEquals("test", userRole);
    }

    @Test
    public void selectGroupMembers() {
        List<GroupMember> groupMembers = groupOperations.selectGroupMembers(groupId);
        assertNotNull(groupMembers);
    }

    @Test
    public void selectGroupMessages() {
        List<GroupMessage> groupMessages = groupOperations.selectGroupMessages(groupId);
        assertNotNull(groupMessages);
    }

    @Test
    public void sendGroupMessage() {
        groupOperations.sendMessageToGroup(groupMessage);
        List<GroupMessage> groupMessages = groupMessageDao.selectGroupMessages(groupId);
        assertEquals("test", groupMessages.get(0).getMessage());
    }

    @Test
    public void addGroupMember() {
        GroupMember newGroupMember = new GroupMember();
        newGroupMember.setMemberId(2);
        newGroupMember.setGroupId(groupId);
        int actualMemberId = groupOperations.addGroupMember(newGroupMember).getMemberId();
        assertEquals(2, actualMemberId);
    }
}
