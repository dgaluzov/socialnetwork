
create table account
(
    ID int auto_increment
        primary key,
    NAME varchar(150) null,
    SURNAME varchar(150) null,
    SECONDNAME varchar(150) null,
    PERSONALPHONE varchar(150) null,
    WORKPHONE varchar(150) null,
    GENDER varchar(6) null,
    BIRTHDAY date null,
    HOMEADDRESS varchar(255) null,
    WORKADDRESS varchar(255) null,
    ABOUT blob null,
    EMAIL varchar(255) null,
    PASSWORD blob null,
    USERPIC blob null,
    RegistrationDate timestamp default CURRENT_TIMESTAMP not null on update CURRENT_TIMESTAMP,
    securityRole varchar(255) null
);

create table friends
(
    id bigint auto_increment
        primary key,
    accountId int null,
    FRIENDID int null,
    RELATION_TYPE varchar(255) null
);

create table groupmembers
(
    id int auto_increment
        primary key,
    Role varchar(255) null,
    GroupID int(255) null,
    MemberId int(255) null
);

create table groupmessage
(
    id int(255) auto_increment
        primary key,
    GroupId int(255) null,
    MemberId int(255) null,
    Message varchar(255) null
);

create table `groups`
(
    id int(255) auto_increment
        primary key,
    Name varchar(255) null,
    Description varchar(255) null,
    CreateDate timestamp null,
    OwnerID int(255) null,
    Photo blob null
);

create table messages
(
    ID int auto_increment
        primary key,
    accountID int null,
    FRIENDID int null,
    MESSAGE varchar(255) null,
    STATUS varchar(255) null,
    MESSAGEDATE timestamp default CURRENT_TIMESTAMP not null on update CURRENT_TIMESTAMP
);

create table persistent_logins
(
    username varchar(100) not null,
    series varchar(64) not null
        primary key,
    token varchar(64) not null,
    last_used timestamp default CURRENT_TIMESTAMP not null on update CURRENT_TIMESTAMP
);

create table phones
(
    id bigint not null,
    accountId bigint null,
    number varchar(255) null
);

