$(function () {
    $("#login-form").validate({

        rules: {
            email: {
                required: true,
                minlength: 4

            },
            password: {
                required: true


            }
        },
        messages: {
            email: {
                required: "This field is required"

            },
            password: {
                required: "This field is required"

            },
            errorElement: "span",

            errorPlacement: function (error, element) {
                element.siblings("label").append(error);
            },
            highlight: function (element) {
                $(element).siblings("label").addClass("error");
            },
            unhighlight: function (element) {
                $(element).siblings("label").removeClass("error");
            }
        }
    });

});

$(function () {
    $("#register-form").validate({

        rules: {
            email: {
                required: true,
                minlength: 4

            },
            password: {
                required: true

            },

            surname: {
                required: true,
                minlength: 4

            },
            pass: {
                required: true,
                minlength: 4

            },
            name: {
                required: true,
                minlength: 4

            }
        },
        messages: {
            email: {
                required: "This field is required"

            },
            password: {
                required: "This field is required"

            },
            errorElement: "span",

            errorPlacement: function (error, element) {
                element.siblings("label").append(error);
            },
            highlight: function (element) {
                $(element).siblings("label").addClass("error");
            },
            unhighlight: function (element) {
                $(element).siblings("label").removeClass("error");
            }
        }
    });

});