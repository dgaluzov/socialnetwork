$(function () {
    let client = null;
    let sender = $("#senderId").text();
    let recipient = $("#recipientId").text();
    let accountName = $("#accountName").text();
    let accountSurname = $("#accountSurname").text();
    let accountPhoto = $("#accountPhoto").text();
    let interlocutorName = $("#interlocutorName").text();
    let interlocutorSurname = $("#interlocutorSurname").text();
    let interlocutorPhoto = $("#interlocutorPhoto").text();

    let block = document.getElementById("chat");


    function connect() {
        if (client != null) {
            client.disconnect();
        }
        let socket = new SockJS('/ws');
        client = Stomp.over(socket);
        client.connect({}, function () {
            client.subscribe("/topic", function (message) {
                showMessage(JSON.parse(message.body));
                block.scrollTop = 99999;

            })
        });
    }

    connect(sender, recipient);


    function getSenderMessage(message) {
        return '<img src="' + accountPhoto + '"> <p>' + accountName + " " + accountSurname + ": " + " " + message.msgTxt
            + '</p>'
    }

    function getRecipientMessage(message) {
        return '<img src="' + interlocutorPhoto + '"> <p>' + interlocutorName + " " + interlocutorSurname + ":" + " " +
            message.msgTxt + '</p>'
    }


    function showMessage(message) {
        if (message.senderId === sender) {
            $('#chat').append(
                '<div class="container">' + getSenderMessage(message) + '</div>'
            );
        } else {
            $('#chat').append(
                '<div class="container darker">' + getRecipientMessage(message) + '</div>'
            );
        }
    }


    $("#sendMessage").click(function () {
        let text = $("#textMessage");
        let message = {
            recipientId: recipient,
            senderId: sender,
            msgTxt: text.val()
        };

        client.send("/app/chat", {}, JSON.stringify(message))
    });

});

