<html>
<body>
<head>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

    <script type="text/javascript" src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="resources/js/easyPaginate.js"></script>
    <link href="resources/css/pagination.css" rel="stylesheet">
    <script type="text/javascript" src="resources/js/easyPaginate.js"></script>
    <script type="text/javascript" src="resources/js/pagination.js"></script>
</head>

<div id="container">

    <h1>Friends</h1>

    <ul id="itemsAccount">
        <jsp:useBean id="friends" scope="request" type="java.util.ArrayList"/>
        <c:choose>
            <c:when test="${friends.size()== 0}">
                <c:out value="No accounts found"/>
            </c:when>
            <c:otherwise>
                <c:forEach var="friend" items="${friends}">
                    <li>


                        <p class="image"><a href="<c:url value="showAccount?id=${friend.id}"/>"><img
                                src="${friend.userPic}"/></a></p>
                        <h3>${friend.name} ${friend.surname}</h3>
                        <c:if test="${friend.friendshipStatus == 'Wants add to friends'}">
                            <a href="<c:url value="confirmFriend?id=${friend.id}"/>">Add
                                to friends</a> </c:if>
                        <c:if test="${friend.friendshipStatus == 'Wants add to friends'}">
                            <a href="<c:url value="deleteFriend?id=${friend.id}"/>">
                                Refuse</a></c:if>
                        <c:if test="${friend.friendshipStatus == 'Your Friend'}">
                            <a href="<c:url value="deleteFriend?id=${friend.id}"/>">
                                Delete</a></c:if>
                    </li>
                </c:forEach>

            </c:otherwise>
        </c:choose>
    </ul>

</div>


</body>
</html>

