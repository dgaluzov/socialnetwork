<%--
  Created by IntelliJ IDEA.
  User: daniilgaluzov
  Date: 03.07.2019
  Time: 17:55
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">


<html>
<head>
    <div class="w3-container">
        <h2>Results</h2>
        <form action="showGroupMembers" method="get">
            <table class="w3-table-all">
                <thead>
                <tr class="w3-grey">
                    <th>Members</th>
                </tr>
                </thead>
                <jsp:useBean id="members" scope="request" type="java.util.ArrayList"/>
                <jsp:useBean id="userRole" scope="request" type="java.lang.String"/>
                <jsp:useBean id="groupId" scope="request" type="java.lang.Integer"/>

                <c:choose>
                    <c:when test="${members.size()== 0}">
                        <c:out value="No members"/>
                    </c:when>
                    <c:otherwise>

                        <c:choose>

                            <c:when test="${userRole =='Admin'}">

                                <c:forEach var="member" items="${members}">
                                    <tr>
                                        <td>

                                            <c:choose>

                                            <c:when test="${member.groupRole =='Wants to enter'}">


                                            <a href="<c:url value="showAccount?id=${member.id}"/>">${member.name}
                                                    ${member.surname} ${member.groupRole}
                                                <a href="<c:url value="confirmGroupMember?id=${member.id}&groupId=${groupId}"/>">
                                                    Confirm</a>

                                                <a href="<c:url
                                            value="deleteGroupMember?id=${member.id}&groupId=${groupId}&isLeave=No"/>">
                                                    Refuse</a>

                                                </c:when>

                                                <c:when test="${member.groupRole =='Admin'}">

                                                <a href="<c:url value="showAccount?id=${member.id}"/>">${member.name}
                                                        ${member.surname} ${member.groupRole} </a>
                                                </c:when>
                                                <c:when test="${member.groupRole =='Moderator'}">
                                                <a href="<c:url value="showAccount?id=${member.id}"/>">${member.name}
                                                        ${member.surname} ${member.groupRole} </a>
                                                <a href="<c:url
                                                value="/deleteGroupMember?id=${member.id}&groupId=${groupId}&isLeave=No"/>">
                                                    Delete</a>
                                                </c:when>
                                                <c:otherwise>

                                                <a href="<c:url value="showAccount?id=${member.id}"/>">${member.name}
                                                        ${member.surname} ${member.groupRole}  </a>

                                                <a href="<c:url value="deleteGroupMember?id=${member.id}&groupId=${groupId}&isLeave=No"/>">
                                                    Delete</a>

                                                <a href="<c:url
                                                value="setModerator?id=${member.id}&groupId=${groupId}"/>">
                                                    Set Moderator</a>

                                                </c:otherwise>
                                                </c:choose>

                                        </td>
                                    </tr>
                                </c:forEach>


                            </c:when>

                            <c:when test="${userRole =='Moderator'}">

                                <c:forEach var="member" items="${members}">
                                    <tr>
                                        <td>


                                            <c:choose>
                                                <c:when test="${member.groupRole =='Wants to enter'}">

                                                    <a href="<c:url value="showAccount?id=${member.id}"/>">${member.name}
                                                            ${member.surname} ${member.groupRole} </a>

                                                    <a href="<c:url value="confirmGroupMember?id=${member.id}&groupId=${groupId}"/>">
                                                        Confirm</a>

                                                    <a href="<c:url value="deleteGroupMember?id=${member.id}&groupId=${groupId}
                                            &isLeave=No"/>">
                                                        Rerfuse </a>

                                                </c:when>
                                                <c:when test="${member.groupRole =='Admin'}">

                                                    <a href="<c:url value="showAccount?id=${member.id}"/>">${member.name}
                                                            ${member.surname} ${member.groupRole} </a> </c:when>
                                                <c:when test="${member.groupRole =='Moderator'}">
                                                    <a href="<c:url value="showAccount?id=${member.id}"/>">${member.name}
                                                            ${member.surname} ${member.groupRole} </a>
                                                </c:when>

                                                <c:otherwise>

                                                    <a href="<c:url value="showAccount?id=${member.id}"/>">${member.name}
                                                            ${member.surname} ${member.groupRole} </a>

                                                    <a href="<c:url
                                                    value="setModerator?id=${member.id}&groupId=${groupId}"/>">
                                                        Set Moderator</a>

                                                    <a href="<c:url
                                            value="deleteGroupMember?id=${member.id}&groupId=${groupId}&isLeave=No
                                            "/>">
                                                        Delete</a>

                                                </c:otherwise>

                                            </c:choose>
                                        </td>
                                    </tr>
                                </c:forEach>


                            </c:when>


                            <c:otherwise>
                                <c:forEach var="member" items="${members}">
                                    <tr>
                                        <td>
                                            <c:choose>
                                                <c:when test="${member.groupRole !='Wants to enter'}">

                                                    <a href="<c:url value="showAccount?id=${member.id}"/>">${member.name}
                                                            ${member.surname} ${member.groupRole} </a>

                                                </c:when>
                                            </c:choose>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </c:otherwise>

                        </c:choose>


                    </c:otherwise>


                </c:choose></table>
        </form>
<body>

</body>
</html>
