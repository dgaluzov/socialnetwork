<%--
  Created by IntelliJ IDEA.
  User: daniilgaluzov
  Date: 01.07.2019
  Time: 16:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<form action="createGroup" method="post">
    <label class="w3-text-teal"> Name</label>
    <label>
        <input class="w3-input" type="text" name="name">
    </label>
    <label class="w3-text-teal"> Description</label>

    <label>
        <input class="w3-input" type="text" name="description">
    </label>

    <label class="w3-text-teal"> Photo</label>
    <label>
        <input class="w3-input" type="text" name="photo" placeholder="Input link">
    </label>


    <button type="submit">Create Group</button>

</form>
