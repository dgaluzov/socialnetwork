<%--
  Created by IntelliJ IDEA.
  User: daniilgaluzov
  Date: 01.07.2019
  Time: 17:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:useBean id="userId" scope="session" type="java.lang.Integer"/>

<jsp:useBean id="groupData" scope="request" type="com.getjavajob.training.web1904.galuzovd.common.Group"/>
<jsp:useBean id="user" scope="request" type="com.getjavajob.training.web1904.galuzovd.common.Account"/>
<jsp:useBean id="owner" scope="request" type="com.getjavajob.training.web1904.galuzovd.common.Account"/>
<jsp:useBean id="isMember" scope="request" type="java.lang.Boolean"/>


<script src="//code.jquery.com/jquery-3.4.1.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
      integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>

<link href="resources/css/profile.css" rel="stylesheet">
<link rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<div class="col-md-15">
    <div class="topnav">
        <c:if test="${userId==user.id}">
            <a href="showAccount?id=${userId}">Home</a></c:if>
        <c:if test="${!isMember}">
            <a href="${pageContext.request.contextPath}/sendRequestToGroup?groupId=${groupData.id}">Send
                Request</a></c:if>
        <c:if test="${isMember}">
            <a href="${pageContext.request.contextPath}/showGroupMembers?id=${groupData.id}">Group
                Members</a></c:if>
        <c:if test="${isMember}">
            <a href="${pageContext.request.contextPath}/deleteGroupMember?id=${userId}&groupId=${groupData.id}&isLeave=Yes">Leave
                group</a></c:if>


    </div>
</div>

<div class="col-md-15">
    <img src="${groupData.photo}">
    <table class="table">

        <tr>
            <th scope="row">Group Name : ${groupData.name}</th>
        </tr>
        <tr>
            <th scope="row">Description : ${groupData.description}</th>
        </tr>
        <tr>
            <th scope="row">Owner : ${owner.name} ${owner.surname}</th>
        </tr>

    </table>
</div>
<div class="col-md-1">
    <form action="showGroup" method="get">
        <table class="w3-table-all">
            <thead>
            <tr class="w3-grey">
                <th>Messages</th>
            </tr>
            </thead>

            <jsp:useBean id="messages" scope="request" class="java.util.ArrayList"/>

            <c:choose>
                <c:when test="${messages.size()== 0}">
                    <c:out value="No messages yet"/>
                </c:when>
                <c:otherwise>
                    <c:forEach var="message" items="${messages}">

                        <tr>
                            <td>


                                <c:out value="${message.name}"/>
                                <c:out value="${message.surname}"/>
                                <c:out value=":"/>
                                <c:out value="${message.groupMessage}"/>


                            </td>
                        </tr>
                    </c:forEach>
                </c:otherwise>
            </c:choose>
        </table>
    </form>
</div>


<form class="w3-container w3-center w3-card-4 w3-light-grey" action="showGroup" method="post">
    <label>
        <label class="w3-text-teal"></label>
        <input class="w3-input" type="text" name="message">
        <input hidden name="id" value="${groupData.id}">
        <button class="w3-btn w3-blue-grey">Send message</button>
    </label>

</form>
