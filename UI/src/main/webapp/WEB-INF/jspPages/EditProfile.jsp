<%@ page import="com.getjavajob.training.web1904.galuzovd.common.Account" %><%--
  Created by IntelliJ IDEA.
  User: daniilgaluzov
  Date: 27.05.2019
  Time: 21:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

<%
    Account userData = (Account) request.getAttribute("userData");
%>
<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>


    <script src="resources/js/loginValidation.js"></script>
    <link href="resources/css/login.css" rel="stylesheet">
    <link href="resources/css/editProfile.css" rel="stylesheet">
    <script src="resources/js/addFields.js"></script>

    <script>
        $(function () {
            $("#login-form").validate({

                rules: {
                    email: {
                        required: true,
                        minlength: 4,

                    },
                    password: {
                        required: true,


                    },
                },
                messages: {
                    email: {
                        required: "This field is required",

                    },
                    password: {
                        required: "This field is required",

                    },
                    errorElement: "span",
                    errorPlacement: function (error, element) {
                        element.siblings("label").append(error);
                    },
                    highlight: function (element) {
                        $(element).siblings("label").addClass("error");
                    },
                    unhighlight: function (element) {
                        $(element).siblings("label").removeClass("error");
                    }
                }
            });

        });


    </script>


    <link href="https://netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet"
          id="bootstrap-css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>


<body>


<div class="container">
    <h2>Edit Profile</h2>
    <form id="login-form" action="${pageContext.request.contextPath}/editProfile" method="POST" role="form"
    >

        <div class="form-group">
            <label for="name">Name:</label>
            <input type="text" class="form-control" id="name" name="name" value="<%=userData.getName()%>">
        </div>

        <div class="form-group">
            <label for="surname">Surname:</label>
            <input type="text" class="form-control" id="surname" name="surname" value="<%=userData.getSurname()%>">
        </div>

        <div class="form-group">
            <label for="password">Password:</label>
            <input type="text" class="form-control" id="password" name="password" value="<%=userData.getPassword()%>">
        </div>
        <div class="form-group">
            <label for="email">Email:</label>
            <input type="text" class="form-control" id="email" name="email" value="<%=userData.getEmail()%>">
        </div>
        <div class="form-group">
            <label for="photo">Image Link:</label>
            <input type="text" class="form-control" id="photo" name="photo" value="<%=userData.getUserPic()%>">
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary pull-left">Save
                changes
            </button>
        </div>
    </form>
</div>


<div class="container">

    <div class="form-group">
        <form method="get" action="${pageContext.request.contextPath}/toXml">
            <button type="button" onclick="location.href='toXml'" class="btn btn-primary pull-left">Export to XML
                File
            </button>
        </form>
    </div>


    <div class="form-group">
        <label for="XML">Import XML

            <div class="input-group input-file" id="XML" name="XML">

                <input type="text" class="form-control" placeholder='Choose a file...'/>
                <span class="input-group-btn">
        		<button class="btn btn-default btn-choose" type="button">Choose</button>
    		</span>
            </div>
        </label>
    </div>

    <div class="form-group">
        <form method="POST" action="${pageContext.request.contextPath}/fromXML" enctype="multipart/form-data">
            <button type="submit" class="btn btn-primary pull-right">Edit From XML</button>
            <button type="reset" class="btn btn-danger">Reset</button>
        </form>
    </div>


</div>


</body>
</html>
