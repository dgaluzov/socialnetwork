<html>
<body>
<head>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

    <script type="text/javascript" src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="resources/js/easyPaginate.js"></script>
    <link href="resources/css/pagination.css" rel="stylesheet">
    <script type="text/javascript" src="resources/js/easyPaginate2.js"></script>
    <script type="text/javascript" src="resources/js/easyPaginate.js"></script>
    <script type="text/javascript" src="resources/js/pagination.js"></script>
</head>

<div id="container">

    <h1>Accounts</h1>

    <ul id="itemsAccount">
        <jsp:useBean id="foundUsers" scope="request" type="java.util.ArrayList"/>
        <c:choose>
            <c:when test="${foundUsers.size()== 0}">
                <c:out value="No accounts found"/>
            </c:when>
            <c:otherwise>
                <c:forEach var="foundUser" items="${foundUsers}">
                    <li>


                        <p class="image"><a href="<c:url value="showAccount?id=${foundUser.id}"/>"><img
                                src="${foundUser.userPic}"

                        /></a></p>
                        <h3>${foundUser.name} ${foundUser.surname}</h3>

                    </li>
                </c:forEach>

            </c:otherwise>
        </c:choose>
    </ul>

</div>


<div id="container2">

    <h1>Groups</h1>


    <ul id="itemsGroup">
        <jsp:useBean id="foundGroups" scope="request" type="java.util.ArrayList"/>
        <c:choose>
            <c:when test="${foundGroups.size()== 0}">
                <c:out value="No groups found"/>
            </c:when>
            <c:otherwise>
                <c:forEach var="foundGroup" items="${foundGroups}">
                    <li>

                        <p class="image"><a href="<c:url value="/showGroup?id=${foundGroup.id}"/>"><img
                                src="${foundGroup.photo}" alt="css/Team default logo.png"/></a></p>
                        <h3>${foundGroup.name}</h3>

                    </li>
                </c:forEach>

            </c:otherwise>
        </c:choose>
    </ul>


</div>

</body>
</html>

