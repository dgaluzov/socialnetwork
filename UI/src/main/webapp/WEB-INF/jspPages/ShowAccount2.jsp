<%--
  Created by IntelliJ IDEA.
  User: daniilgaluzov
  Date: 01.08.2019
  Time: 21:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <html>
    <head>
        <jsp:useBean id="userId" scope="session" type="java.lang.Integer"/>
        <jsp:useBean id="friendStatus" scope="request" type="java.lang.String"/>
        <jsp:useBean id="userData" scope="request" type="com.getjavajob.training.web1904.galuzovd.common.Account"/>


        <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
        <script src="//code.jquery.com/jquery-3.4.1.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet"
              id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>

        <link href="resources/css/profile.css" rel="stylesheet">
        <link rel="stylesheet"
              href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link id="contextPathHolder" data-contextPath="${pageContext.request.contextPath}"/>

        <div class="container">
            <div class="row profile">
                <div class="col-md-15">
                    <div class="btn-group btn-group-justified">


                    </div>
                    <div class="col-md-15">
                        <div class="topnav">
                            <c:if test="${userId==userData.id}">
                                <a href="">Home</a></c:if>
                            <c:if test="${userId == userData.id}">
                                <a href="${pageContext.request.contextPath}/showFriends">My friends</a></c:if>
                            <c:if test="${userId==userData.id}">
                                <a href="${pageContext.request.contextPath}/editProfile">
                                    Edit Profile</a></c:if>
                            <c:if test="${userId==userData.id}">
                                <a href="${pageContext.request.contextPath}/createGroup" class=>Create group</a></c:if>
                            <a href="${pageContext.request.contextPath}/invalidateSession" class=>Log out</a>

                            <div class="search-container">
                                <form action="${pageContext.request.contextPath}/searchAccount">
                                    <input type="text" id="search" placeholder="Search.." name="search">
                                    <button type="submit"><i class="fa fa-search"></i></button>
                                </form>
                            </div>
                        </div>
                    </div>


                    <div class="profile-sidebar">

                        <div class="profile-userpic">
                            <img src="${userData.userPic}" class="img-responsive"
                                 alt="">
                        </div>

                        <div class="profile-usertitle">
                            <div class="profile-usertitle-name">
                                ${userData.name} ${userData.surname}
                            </div>

                        </div>
                        <!-- END SIDEBAR USER TITLE -->
                        <!-- SIDEBAR BUTTONS -->
                        <div class="profile-userbuttons">

                            <c:if test="${userId!=userData.id}">
                                <c:if test="${friendStatus == ''}">
                                    <button type="button"
                                            onclick="location.href='${pageContext.request.contextPath}/addFriend?id=${userData.id}'"
                                            class="btn btn-primary">Add to friend
                                    </button>
                                </c:if></c:if>
                            <c:if test="${friendStatus == 'Accepted'}">
                                <button type="button"
                                        onclick="location.href='${pageContext.request.contextPath}/deleteFriend?id=${userData.id}'"
                                        class="btn btn-primary">Delete friend
                                </button>
                            </c:if>
                            <c:if test="${friendStatus=='Accepted'}">
                                <button type="button"
                                        onclick="location.href='${pageContext.request.contextPath}/message?id=${userData.id}'"
                                        class="btn
                            btn-primary">Message
                                </button>
                            </c:if>


                            <script>

                                $(document).ready(function () {
                                    $("#search").autocomplete({
                                            source: function (request, response) {
                                                $.post('<c:url value="/searchAJAX?search="/>' + request.term,
                                                    function (data) {
                                                        response($.map(data, function (val, i) {
                                                            return {label: val.name + ' ' + val.surname}
                                                        }));

                                                    })
                                            }
                                        }
                                    )
                                });
                            </script>

                        </div>

                    </div>
                </div>
                <div class="col-md-15">
                    <div class="profile-content">

                    </div>
                    <div class="col-md-10">
                    </div>
                </div>
            </div>

            <br>
            <br>
        </div>

    </head>

    </html>
