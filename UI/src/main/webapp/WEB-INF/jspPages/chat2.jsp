<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:useBean id="messages" scope="request" class="java.util.ArrayList"/>
<jsp:useBean id="interlocutor" scope="request"
             class="com.getjavajob.training.web1904.galuzovd.common.Account"/>
<jsp:useBean id="account" scope="request"
             class="com.getjavajob.training.web1904.galuzovd.common.Account"/>
<html>
<head>

    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" type="text/css"/>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

    <div hidden id="senderId">${account.id}</div>
    <div hidden id="recipientId">${interlocutor.id}</div>
    <div hidden id="interlocutorPhoto">${interlocutor.userPic}</div>
    <div hidden id="accountPhoto">${account.userPic}</div>
    <div hidden id="accountSurname">${account.surname}</div>
    <div hidden id="accountName">${account.name}</div>
    <div hidden id="interlocutorName">${interlocutor.name}</div>
    <div hidden id="interlocutorSurname">${interlocutor.surname}</div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sockjs-client/1.2.0/sockjs.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/stomp.js/2.3.3/stomp.js"></script>
    <link href="${pageContext.request.contextPath}/resources/css/chat.css" rel="stylesheet">
    <script src="${pageContext.request.contextPath}/resources/js/websocket.js"></script>

</head>
<body>
<div class="chat" id="chat">
    <c:forEach var="message" items="${messages}">
        <c:choose>
            <c:when test="${message.status == 'Message sent'}">
                <div class="container">

                    <img src="${account.userPic}" alt="Avatar">
                    <p><c:out value="${account.name}"/>
                        <c:out value="${account.surname}"/>
                        <c:out value=":"/>
                        <c:out value="${message.messageText}"/></p>

                </div>
            </c:when>
            <c:otherwise>
                <div class="container darker">
                    <img src="${interlocutor.userPic}" alt="Avatar" class="right">
                    <p class="right"><c:out value="${interlocutor.name}"/>
                        <c:out value="${interlocutor.surname}"/>
                        <c:out value=":"/>
                        <c:out value="${message.messageText}"/></p>


                </div>

            </c:otherwise>
        </c:choose>
    </c:forEach>
</div>

<div class="form-group">
    <label for="textMessage"></label>
    <textarea id="textMessage" class="form-control" name="message" rows="4" required
              placeholder="Type your message here"></textarea>
</div>
<a id="sendMessage" class="btn btn-primary ">Send message</a>


</body>
</html>
