<%--
  Created by IntelliJ IDEA.
  User: daniilgaluzov
  Date: 27.05.2019
  Time: 20:54
  To change this template use File | Settings | File Templates.
--%>
<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<head>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
    <%@ page contentType="text/html;charset=UTF-8" language="java" %>


<body>

<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>

<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="resources/js/login.js"></script>
<script src="resources/js/loginValidation.js"></script>
<link href="resources/css/login.css" rel="stylesheet">


<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-login">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-6">
                            <a href="#" class="active" id="login-form-link">Login</a>
                        </div>
                        <div class="col-xs-6">
                            <a href="#" id="register-form-link">Register</a>
                        </div>
                    </div>
                    <hr>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <form id="login-form" action="${pageContext.request.contextPath}/login" method="post"
                                  role="form" style="display: block;">
                                <div class="form-group ">
                                    <input type="text" name="username" id="emailLogin" tabindex="1" class="form-control"
                                           placeholder="Email" value="">
                                </div>
                                <div class="form-group">
                                    <input type="password" name="password" id="password" tabindex="2"
                                           class="form-control" placeholder="Password">
                                </div>
                                <div class="form-group text-center">
                                    <input type="checkbox" tabindex="3" class="" name="remember-me" id="remember-me">
                                    <label for="remember-me"> Remember Me</label>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-6 col-sm-offset-3">
                                            <button type="submit" class="form-control btn btn-login"
                                            >Log in
                                            </button>

                                        </div>
                                    </div>
                                </div>

                            </form>
                            <form id="register-form" action="${pageContext.request.contextPath}/registration"
                                  method="post" role="form" style="display: none;">
                                <div class="form-group">
                                    <input type="text" name="name" id="username" tabindex="1" class="form-control"
                                           placeholder="Name" value="">
                                </div>
                                <div class="form-group">
                                    <input type="text" name="surname" id="surname" tabindex="1" class="form-control"
                                           placeholder="Surname" value="">
                                </div>
                                <div class="form-group">
                                    <input type="email" name="email" id="email" tabindex="1" class="form-control"
                                           placeholder="Email Address" value="">
                                </div>
                                <div class="form-group">
                                    <input type="password" name="pass" id="pass" tabindex="2"
                                           class="form-control" placeholder="Password">
                                </div>
                                <div class="form-group">
                                    <input type="password" name="imageLink" id="confirm-password" tabindex="2"
                                           class="form-control" placeholder="Image Link">
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-6 col-sm-offset-3">
                                            <input type="submit" name="register-submit" id="register-submit"
                                                   tabindex="4" class="form-control btn btn-register"
                                                   value="Register Now">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


</body>
</head>