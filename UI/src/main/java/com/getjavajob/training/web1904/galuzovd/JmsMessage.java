package com.getjavajob.training.web1904.galuzovd;

import java.util.Objects;

public class JmsMessage {

    private String email;
    private String messageText;
    public JmsMessage() {
    }

    public JmsMessage(String email, String messageText) {
        this.email = email;
        this.messageText = messageText;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        JmsMessage that = (JmsMessage) o;
        return Objects.equals(email, that.email) &&
                Objects.equals(messageText, that.messageText);
    }

    @Override
    public int hashCode() {
        return Objects.hash(email, messageText);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }
}
