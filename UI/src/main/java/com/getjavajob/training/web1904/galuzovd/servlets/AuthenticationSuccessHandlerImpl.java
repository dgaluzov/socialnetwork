package com.getjavajob.training.web1904.galuzovd.servlets;

import com.getjavajob.training.web1904.galuzovd.common.Account;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

class AuthenticationSuccessHandlerImpl implements AuthenticationSuccessHandler {
    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
                                        Authentication authentication) throws IOException {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        SecurityAccount securityAccount = (SecurityAccount) auth.getPrincipal();
        Account account = securityAccount.getAccount();
        httpServletRequest.getSession().setAttribute("userId", account.getId());
        httpServletResponse.sendRedirect(httpServletRequest.getContextPath() + "/showAccount?id=" + account.getId());
    }
}
