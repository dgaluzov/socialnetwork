package com.getjavajob.training.web1904.galuzovd.servlets;

public class JsonMessage {
    private String senderId;
    private String recipientId;
    private String msgTxt;

    public JsonMessage(String senderId, String recipientId, String msgText) {
        this.senderId = senderId;
        this.recipientId = recipientId;
        this.msgTxt = msgText;
    }

    public String getSenderId() {
        return senderId;
    }

    public String getRecipientId() {
        return recipientId;
    }

    public String getMsgTxt() {
        return msgTxt;
    }
}
