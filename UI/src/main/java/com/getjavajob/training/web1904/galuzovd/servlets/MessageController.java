package com.getjavajob.training.web1904.galuzovd.servlets;//todo Boot websocket кириллица

import com.getjavajob.training.web1904.galuzovd.common.Account;
import com.getjavajob.training.web1904.galuzovd.common.Message;
import com.getjavajob.training.web1904.galuzovd.service.AccountOperations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class MessageController {

    @Autowired
    private AccountOperations accountOperations;

    @RequestMapping(value = "/message", method = RequestMethod.GET)
    public ModelAndView message(HttpSession session, @RequestParam("id") int recipientId) {
        ModelAndView modelAndView = new ModelAndView("chat2");
        int userId = (int) session.getAttribute("userId");
        Account userData = accountOperations.getAccount(recipientId);
        Account account = accountOperations.getAccount(userId);
        modelAndView.addObject("interlocutor", userData);
        modelAndView.addObject("account", account);
        session.setAttribute("forMessageRecipientId", recipientId);
        List<Message> messages = accountOperations.selectMessages(userId, recipientId);
        modelAndView.addObject("messages", messages);
        return modelAndView;
    }

    @MessageMapping("/chat")
    @SendTo("/topic")
    public JsonMessage getMessage(JsonMessage message, SimpMessageHeaderAccessor simpMessageHeaderAccessor) {
        int senderId = (int) simpMessageHeaderAccessor.getSessionAttributes().get("userId");
        int recipientId = Integer.parseInt(message.getRecipientId());
        accountOperations.sendMessageSender(senderId, recipientId, message.getMsgTxt());
        accountOperations.sendMessageRecipient(senderId, recipientId, message.getMsgTxt());
        return new JsonMessage(message.getSenderId(), message.getRecipientId(), message.getMsgTxt());
    }
}
