package com.getjavajob.training.web1904.galuzovd.servlets;

import com.getjavajob.training.web1904.galuzovd.common.Account;
import com.getjavajob.training.web1904.galuzovd.common.Group;
import com.getjavajob.training.web1904.galuzovd.common.GroupMember;
import com.getjavajob.training.web1904.galuzovd.common.GroupMessage;
import com.getjavajob.training.web1904.galuzovd.service.AccountOperations;
import com.getjavajob.training.web1904.galuzovd.service.GroupOperations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Controller
public class GroupController {

    @Autowired
    private GroupOperations groupOperations;

    @Autowired
    private AccountOperations accountOperations;

    @RequestMapping(value = "/showGroupMembers", method = RequestMethod.GET)
    public ModelAndView showGroupMembers(HttpSession session, @RequestParam("id") int groupId) {
        ModelAndView modelAndView = new ModelAndView("ShowGroupMembers");
        int userId = (int) session.getAttribute("userId");
        String userRole = groupOperations.selectUserRole(userId, groupId);
        List<GroupMember> members = groupOperations.selectGroupMembers(groupId);
        List<Account> accounts = new ArrayList<>();
        Account member;
        for (GroupMember groupMember : members) {
            member = accountOperations.getAccount(groupMember.getMemberId());
            member.setGroupRole(groupMember.getRole());
            member.setGroupId(groupMember.getGroupId());
            accounts.add(member);
        }
        modelAndView.addObject("members", accounts);
        if (userRole != null) {
            modelAndView.addObject("userRole", userRole);
        } else {
            modelAndView.addObject("userRole", "Spectator");
        }
        modelAndView.addObject("groupId", groupId);
        return modelAndView;
    }

    @RequestMapping(value = "/showGroup", method = RequestMethod.GET)
    public ModelAndView showGroup(HttpSession session, @RequestParam("id") int groupId) {
        ModelAndView modelAndView = new ModelAndView("ShowGroup");
        int userId = (int) session.getAttribute("userId");
        Account user = accountOperations.getAccount(userId);
        modelAndView.addObject("user", user);
        user.setGroupRole("Spectator");
        session.setAttribute("groupId", groupId);
        Group groupData = groupOperations.getGroup(groupId);
        int ownerId = groupData.getOwnerId();
        modelAndView.addObject("groupData", groupData);
        Account owner = accountOperations.getAccount(ownerId);
        modelAndView.addObject("owner", owner);
        List<GroupMember> members = groupOperations.selectGroupMembers(groupId);
        List<GroupMessage> messages = groupOperations.selectGroupMessages(groupId);
        boolean isMember = false;
        for (GroupMember groupMember : members) {
            if (groupMember.getMemberId() == userId) {
                isMember = true;
            }
        }
        List<Account> accounts = new ArrayList<>();
        Account member;
        for (GroupMember groupMember : members) {
            member = accountOperations.getAccount(groupMember.getMemberId());
            member.setGroupRole(groupMember.getRole());
            member.setGroupId(groupMember.getGroupId());
            accounts.add(member);
        }
        List<Account> accountsForMessage = new ArrayList<>();
        Account accountWithMessage;
        for (GroupMessage message : messages) {
            accountWithMessage = accountOperations.getAccount(message.getMemberId());
            accountWithMessage.setGroupMessage(message.getMessage());
            accountWithMessage.setGroupId(message.getGroupId());
            accountsForMessage.add(accountWithMessage);
        }
        modelAndView.addObject("messages", accountsForMessage);
        modelAndView.addObject("members", members);
        modelAndView.addObject("accounts", accounts);
        modelAndView.addObject("isMember", isMember);
        return modelAndView;
    }

    @RequestMapping(value = "/showGroup", method = RequestMethod.POST)
    public ModelAndView groupMessage(HttpSession session, @RequestParam("id") int groupId,
                                     @RequestParam("message") String message) {
        int id = (int) session.getAttribute("userId");
        GroupMessage groupMessage = new GroupMessage(id, groupId, message);
        groupOperations.sendMessageToGroup(groupMessage);
        return new ModelAndView("redirect:showGroup?id=" + groupId);
    }

    @RequestMapping(value = "/setModerator", method = RequestMethod.GET)
    public ModelAndView setModerator(@RequestParam("id") int groupId, @RequestParam(
            "id") int memberId) {
        ModelAndView modelAndView = new ModelAndView("redirect:showGroupMembers?id=" + groupId);
        groupOperations.updateMembersRole(memberId, "Moderator");
        return modelAndView;
    }

    @RequestMapping(value = "/sendRequestToGroup", method = RequestMethod.GET)
    public ModelAndView sendRequestToGroup(HttpSession session, @RequestParam("groupId") int groupId) {
        int userId = (int) session.getAttribute("userId");
        GroupMember groupMember = new GroupMember("Wants to enter", groupId, userId);
        groupOperations.addGroupMember(groupMember);
        return new ModelAndView("redirect:showGroup?id=" + groupId);
    }

    @RequestMapping(value = "/deleteGroupMember", method = RequestMethod.GET)
    public ModelAndView deleteGroupMember(@RequestParam("id") int memberId,
                                          @RequestParam("groupId") int groupId, @RequestParam("isLeave") String isLeave) {
        groupOperations.deleteGroupMember(groupId, memberId);
        if (isLeave.equals("Yes")) {
            return new ModelAndView("redirect:showGroup?id=" + groupId);
        } else {
            return new ModelAndView("redirect:showGroupMembers?id=" + groupId);
        }
    }

    @RequestMapping(value = "/editGroup", method = RequestMethod.GET)
    public ModelAndView editGroup(HttpSession session) {
        int id = (int) session.getAttribute("userId");
        ModelAndView modelAndView = new ModelAndView("EditGroup?id =" + id);
        Group groupData = groupOperations.getGroup(id);
        modelAndView.addObject("userData", groupData);
        return modelAndView;
    }

    @RequestMapping(value = "/confirmGroupMember", method = RequestMethod.GET)
    public ModelAndView confirmGroupMember(@RequestParam("id") int memberId,
                                           @RequestParam("groupId") int groupId) {
        groupOperations.updateMembersRole(memberId, "Member");
        return new ModelAndView("/showGroupMembers?id=" + groupId);
    }

    @RequestMapping(value = "/createGroup", method = RequestMethod.GET)
    public ModelAndView createGroup() {
        return new ModelAndView("CreateGroup");
    }

    @RequestMapping(value = "/createGroup", method = RequestMethod.POST)
    public ModelAndView doCreateGroup(HttpSession session,
                                      @RequestParam("description") String description,
                                      @RequestParam("name") String name,
                                      @RequestParam("photo") String photo) {
        if (name == null || name.trim().isEmpty()) {
            return new ModelAndView("GroupFailed");
        }
        int userId = (int) session.getAttribute("userId");
        if (photo.equals("")) {
            photo = "https://neilpatel.com/wp-content/uploads/2017/02/facebookgroup.jpg";
        }
        Group group = new Group(userId, name, description, photo);
        int groupId = groupOperations.createGroup(group);
        GroupMember groupMember = new GroupMember("Admin", groupId, userId);
        groupOperations.addGroupMember(groupMember);
        return new ModelAndView("GroupCreateSuccess");
    }
}