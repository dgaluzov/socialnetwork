package com.getjavajob.training.web1904.galuzovd.servlets;

import com.getjavajob.training.web1904.galuzovd.JmsMessage;
import com.getjavajob.training.web1904.galuzovd.common.Account;
import com.getjavajob.training.web1904.galuzovd.common.Group;
import com.getjavajob.training.web1904.galuzovd.dao.GroupDao;
import com.getjavajob.training.web1904.galuzovd.service.AccountOperations;
import com.thoughtworks.xstream.XStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletContextListener;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

import static com.getjavajob.training.web1904.galuzovd.Application.ORDER_QUEUE;
import static org.springframework.http.HttpHeaders.CONTENT_DISPOSITION;
import static org.springframework.http.MediaType.APPLICATION_XML;

@Controller
public class UIController {
    private static final Logger logger = LoggerFactory.getLogger(UIController.class);

    @Autowired
    private JmsTemplate jmsTemplate;
    @Autowired
    private GroupDao groupDao;
    @Autowired
    private AccountOperations accountOperations;

    private static SecurityAccount getCurrentUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return auth instanceof AnonymousAuthenticationToken ? null : (SecurityAccount) auth.getPrincipal();
    }

    @RequestMapping(value = "/")
    public ModelAndView index(HttpSession session) {
        return new ModelAndView("redirect:/login");
    }

    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public ModelAndView registration() {
        return new ModelAndView("Registration");
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView getLogin(HttpSession session) {
        SecurityAccount securityAccount = getCurrentUser();
        if (securityAccount != null) {
            String username = securityAccount.getUsername();
            securityAccount.setAccount(accountOperations.searchUser(username));
            Account userAccount = securityAccount.getAccount();
            session.setAttribute("userId", userAccount.getId());
            return new ModelAndView("redirect:/showAccount?id=" + userAccount.getId());
        } else {
            return new ModelAndView("index");
        }
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public ModelAndView doRegistration(@RequestParam("email") String email, @RequestParam("name") String name,
                                       @RequestParam("pass") String password, @RequestParam("surname") String surname,
                                       @RequestParam("imageLink") String imageLink) {
        if (accountOperations.isDuplicate(email)) {
            return new ModelAndView("RegistrationFailed(Duplicates)");
        }
        int id = accountOperations.createAccount(name, surname, email, password, imageLink);
        if (imageLink.equals("")) {
            imageLink = "https://internetuniversitet.ru/wp-content/uploads/2017/11/no-avatar.png";
        }
        accountOperations.updateProfile(id, name, surname, email, password, imageLink);
        jmsTemplate.convertAndSend(ORDER_QUEUE, new JmsMessage(email, "you successfully registered"));
        return new ModelAndView("RegistrationSuccess");
    }

    @RequestMapping(value = "/showAccount", method = RequestMethod.GET)
    public ModelAndView showAccount(HttpSession session, @RequestParam("id") int id) {
        ModelAndView modelAndView = new ModelAndView("ShowAccount2");
        int friendId = (int) session.getAttribute("userId".trim());
        Account userData = accountOperations.getAccount(id);
        modelAndView.addObject("userData", userData);
        String friendStatus;
        String result = accountOperations.selectRelationType(id, friendId);
        switch (result) {
            case "requestReceived":
                friendStatus = "Request send";
                break;
            case "":
                friendStatus = "";
                break;
            case "requestSent":
                friendStatus = "not";
                break;
            default:
                friendStatus = "Accepted";
                break;
        }
        modelAndView.addObject("friendStatus", friendStatus);
        return modelAndView;
    }

    @RequestMapping(value = "/searchAccount", method = RequestMethod.GET)
    public ModelAndView searchAccount(@RequestParam("search") String search) {
        ModelAndView modelAndView = new ModelAndView("ShowSearchResults2");
        List<Account> foundUsers = accountOperations.searchUsers(search);
        modelAndView.addObject("foundUsers", foundUsers);
        List<Group> foundGroups = groupDao.searchGroup("%" + search + "%");
        modelAndView.addObject("foundGroups", foundGroups);
        return modelAndView;
    }

    @RequestMapping(value = "/showAccount", method = RequestMethod.POST)
    public ModelAndView doShowAccount(@RequestParam("search") String search) {
        return new ModelAndView("redirect:searchAccount?search=" + search);
    }

    @RequestMapping(value = "/searchAJAX")
    @ResponseBody
    public List<Account> ajaxshowaccount(@RequestParam("search") String search) {
        return accountOperations.searchUsers(search);
    }

    @RequestMapping(value = "/showFriends", method = RequestMethod.GET)
    public ModelAndView showFriends(HttpSession session) {
        int id = (int) session.getAttribute("userId".trim());
        List<Account> friends = accountOperations.selectFriends(id);
        for (Account friend : friends) {
            String status = accountOperations.selectRelationType(id, friend.getId());
            switch (status) {
                case "Accepted":
                    friend.setFriendshipStatus("Your Friend");
                    break;
                case "requestReceived":
                    friend.setFriendshipStatus("Wants add to friends");
                    break;
                case "requestSent":
                    friend.setFriendshipStatus("Request send");
                    break;
            }
        }
        ModelAndView modelAndView = new ModelAndView("Friends");
        return modelAndView.addObject("friends", friends);
    }

    @RequestMapping(value = "/editProfile", method = RequestMethod.GET)
    public ModelAndView editProfile(HttpSession session) {
        int id = (int) session.getAttribute("userId");
        ModelAndView modelAndView = new ModelAndView("EditProfile");
        Account userData = accountOperations.getAccount(id);
        modelAndView.addObject("userData", userData);
        return modelAndView;
    }

    @RequestMapping(value = "/editProfile", method = RequestMethod.POST)
    public ModelAndView doEditProfile(HttpSession session, @RequestParam("email") String email,
                                      @RequestParam("name") String name,
                                      @RequestParam("password") String password,
                                      @RequestParam("surname") String surname,
                                      @RequestParam("photo") String imageLink) {
        int id = (int) session.getAttribute("userId");
        accountOperations.updateProfile(id, name, surname, email, password, imageLink);
        return new ModelAndView("EditProfileSuccess");
    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(value = "/deleteFriend", method = RequestMethod.GET)
    public ModelAndView deleteFriend(HttpSession session, @RequestParam("id") int recepientId) {
        int id = (int) session.getAttribute("userId");
        accountOperations.deleteFriend(id, recepientId);
        return new ModelAndView("redirect:showFriends");
    }

    @RequestMapping(value = "/deleteAccount", method = RequestMethod.GET)
    public ModelAndView deleteAccount(HttpSession session) {
        int id = (int) session.getAttribute("userId");
        session.invalidate();
        accountOperations.deleteAccount(id);
        return new ModelAndView("redirect:/index");
    }

    @RequestMapping(value = "/confirmFriend", method = RequestMethod.GET)
    public ModelAndView confirmFriend(HttpSession session, @RequestParam("id") int recepientId
    ) {
        int id = (int) session.getAttribute("userId");
        accountOperations.confirmFriend(id, recepientId);
        return new ModelAndView("redirect:showFriends");
    }

    @RequestMapping(value = "/addFriend", method = RequestMethod.GET)
    public ModelAndView addFriend(HttpSession session, @RequestParam("id") int recepientId
    ) {
        int id = (int) session.getAttribute("userId");
        accountOperations.sendRequest(id, recepientId);
        return new ModelAndView("redirect:showAccount?id=" + recepientId);
    }

    @RequestMapping(value = "/fromXML", method = RequestMethod.POST)
    public ModelAndView fromXML(@RequestParam("XML") MultipartFile file
    ) {
        ModelAndView modelAndView = new ModelAndView("redirect:editProfile");
        XStream xstream = new XStream();
        xstream.processAnnotations(Account.class);
        try {
            Account xmlAccount = (Account) xstream.fromXML(new String(file.getBytes()));
            accountOperations.updateProfile(xmlAccount.getId(), xmlAccount.getName(), xmlAccount.getSurname(),
                    xmlAccount.getEmail(), xmlAccount.getPassword(), xmlAccount.getUserPic());
        } catch (IOException e) {
            logger.error("Wrong XML", e);
        }
        return modelAndView;
    }

    @RequestMapping(value = "/toXml", method = RequestMethod.GET)
    public void createAccountXml(HttpSession session, HttpServletResponse response) {
        XStream xstream = new XStream();
        xstream.processAnnotations(Account.class);
        response.addHeader(CONTENT_DISPOSITION, "attachment;filename=account.xml");
        response.setContentType(String.valueOf(APPLICATION_XML));
        Account account = accountOperations.getAccount((Integer) session.getAttribute("userId"));
        try {
            response.getOutputStream().write(xstream.toXML(account).getBytes());
        } catch (IOException e) {
            logger.error("Exeption", e);
        }
        try {
            response.flushBuffer();
        } catch (IOException e) {
            logger.error("Exeption", e);
        }
    }

    public class MyAppServletContextListener
            implements ServletContextListener {
    }
}




